Epiphany Box
==========

Epiphany Box is a preconfigured Vagrant Box with a full array of LAMP Stack features to get you up and running with Vagrant in no time.
No provisioning tools or setup is really even required with Epiphany Box.


## Quick Start

```bash
$ git clone git@bitbucket.org:epiphanyinternaltools/epiphany-vagrant-box.git
$ cd epiphany-vagrant-box
$ vagrant up
```

```bash
$ vagrant ssh
$ addwebsite test.dev.epiphanytools.co.uk
--
From your machine cd into public/
$ git clone git@bitbucket.org:epiphanyinternaltools/projectrepo.git test.dev.epiphanytools.co.uk
- Visit test.dev.epiphanytools.co.uk
```
## Configuration Options
**config.yaml**
```
ip: 192.168.10.10
hostname: epiphanybox
power:
   - memory: 2048
     cpus: 4
# blackfire:
#    - id: foo
#      token: bar
#      client-id: foo
#      client-token: bar
```



## Useful Aliases
```bash
alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'
alias dev='php bin/console --env=dev'
alias prod='php bin/console --env=prod'
alias cc='php bin/console cache:clear'
alias console="php bin/console"
alias entity="php bin/console doctrine:generate:entity"
alias bundle="php bin/console generate:bundle"
alias cc="php bin/console cache:clear"
alias diff="php bin/console doctrine:migrations:diff"
alias migrate="php bin/console doctrine:migrations:migrate"
alias fixload="php bin/console doctrine:fixtures:load"
alias controller="php bin/console generate:controller"
alias routes="php bin/console router:debug"
alias server="php bin/console server:run"
alias sf2='php app/console'
alias desc='php "./vendor/phpspec/phpspec/bin/phpspec" desc'
alias spec='php "./vendor/phpspec/phpspec/bin/phpspec" run -f pretty'
alias ldb='mysql -u root -proot symfony'
```

## Features

### System Stuff

- Ubuntu 14.04 LTS (Trusty Tahr)
- PHP 7.1
- Ruby 2.4
- Vim
- Git
- cURL
- GD and Imagick
- Composer
- Beanstalkd
- Node
- NPM
- Mcrypt

### Database Stuff
- MySQL
- SQLite

### Caching Stuff

- Redis
- Memcache and Memcached

### Node Stuff

- Grunt
- Bower
- Yeoman
- Gulp
- Browsersync
- PM2


## Gettting Started

* Download and Install Vagrant
* Download and Install VirtualBox
* Clone the Epiphany Box GitHub Repository
* Run Vagrant Up
* Access Your Project at  http://192.168.33.10/


## Basic Vagrant Commands


### Start or resume your server
```bash
vagrant up
```

### Pause your server
```bash
vagrant suspend
```

### Delete your server
```bash
vagrant destroy
```

### SSH into your server
```bash
vagrant ssh
```
### Check all your running boxes
```bash
vagrant global-status
```
## Database Access
### MySQL 

- Hostname: localhost or 127.0.0.1
- Username: root
- Password: root
- Database: epiphany


## Setting a Hostname

Add the following record on your hosts file

```bash
192.168.33.10 test.dev.epiphanytools.co.uk
```


You could also a Vagrant Plugin like *Vagrant Hostmanager* to automatically update your host file when you run Vagrant Up. 

## Running multiple Boxes

You can run multiple boxes in the same machine.
Example Configuration

```bash
config.vm.network "private_network", ip: "192.168.33.33"
config.vm.hostname = "fusion"
config.vm.network :forwarded_port, guest: 22, host: 2201, id: "ssh", auto_correct: true
```

## Adding more power to your Box
Epiphany Vagrant Boxs
```bash
config.vm.provider "virtualbox" do |vb|
        vb.memory = "2048"
        vb.cpus = "4"
    end
```