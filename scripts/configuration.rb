class EpiphanyBox
    def EpiphanyBox.configure(config, settings)

        scriptDir = File.dirname(__FILE__)

        if settings.has_key?("power")
            config.vm.provider "virtualbox" do |vb|
                vb.memory = settings["power"][0]["memory"]
                vb.cpus = settings["power"][0]["cpus"]
            end
        end

        # Configure Blackfire.io
        if settings.has_key?("blackfire")
            config.vm.provision "shell" do |s|
                s.path = scriptDir + "/blackfire.sh"
                s.args = [
                    settings["blackfire"][0]["id"],
                    settings["blackfire"][0]["token"],
                    settings["blackfire"][0]["client-id"],
                    settings["blackfire"][0]["client-token"]
                ]
            end
        end
    end
end